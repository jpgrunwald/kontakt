from collections import defaultdict
import re


class GehoertNichtInGruppe(ValueError):
    '''
    '''

class NummernGruppe:
    def __init__(self, initialwert:str):
        self._werte = []
        self._ist_alphanumerisch = False
        self._wertabstand = None

        self.hinzufuegen(initialwert)

    def __str__(self):
        if self._ist_alphanumerisch:
            basis_1, suffix_1 = self._teile_alphanum(self._werte[0])
            _, suffix_2 = self._teile_alphanum(self._werte[-1])

            if len(self._werte) > 1:
                return f'{basis_1}{suffix_1}-{suffix_2}'
            return self._werte[0]

        if len(self._werte) > 1:
            bereich_str = f'{self._werte[0]}-{self._werte[-1]}'
            if self._wertabstand == 1:
                return bereich_str

            if int(self._werte[0]) % 2:
                return f'{bereich_str} (unger.)'
            return f'{bereich_str} (ger.)'

        return self._werte[0]

    @staticmethod
    def _teile_alphanum(wert: str) -> tuple[str, str]:
        matches = re.findall(r'([0-9]+)([a-z]+)', wert)
        return matches[0]

    def _ist_teil_der_gruppe(self, wert:str) -> bool:
        # Der erste Wert kann beliebig gewählt werden
        if not self._werte:
            self._ist_alphanumerisch = not wert.isnumeric()
            return True

        # Alphanumerische Hausnummern sind etwas schwieriger.
        # Die Basis muss übereinstimmen, und es sind keine Lücken
        # erlaubt.

        if self._ist_alphanumerisch:
            bestands_basis, bestands_suffix = self._teile_alphanum(self._werte[-1])
            basis, suffix = self._teile_alphanum(wert)

            if bestands_basis != basis:
                return False

            return (ord(suffix) - ord(bestands_suffix)) == 1

        if not self._ist_alphanumerisch and not wert.isnumeric():
            return False

        # Der zweite Wert entscheidet über die (nummerische) Gruppenart.
        # Direkt aufeinanderfolgende Hausnummern oder zumindest alle
        # aufeinanderfolgenden geraden/ungeraden sind zulässig.
        if not self._wertabstand:
            wertabstand = int(wert) - int(self._werte[-1])
            if wertabstand > 2:
                return False

            self._wertabstand = wertabstand
            return True

        return int(wert) == int(self._werte[-1]) + self._wertabstand

    def hinzufuegen(self, wert:str):
        if self._ist_teil_der_gruppe(wert):
            self._werte.append(wert)
        else:
            raise GehoertNichtInGruppe(wert)

def sortiere_hausnummern(hausnummern):
    gesplittete_nummern = [(int(a), b) for a, b in [
        re.findall(r'([0-9]+)([a-z]+)?', hnr)[0]
        for hnr in hausnummern
    ]]

    return [
        f'{a}{b}'
        for a, b in sorted(gesplittete_nummern)
    ]

def hausnummern_gruppieren(hausnummern):
    gruppen = []
    for nr in sortiere_hausnummern(hausnummern):
        try:
            gruppen[-1].hinzufuegen(nr)
        except (IndexError, GehoertNichtInGruppe):
            gruppen.append(NummernGruppe(nr))
    return gruppen


def strassennamen_zusammenfassen(strassen: list[str]) -> list[str]:
    nummern_zuordnung = defaultdict(list)
    for strasse in strassen:
        name, hnr = strasse.rsplit(' ', 1)
        name = re.sub(r'(.*)stra(?:ss|ß)e$', r'\1str.', name.lower())
        name = re.sub(r'\bstrasse\b', r'straße', name)
        nummern_zuordnung[name].append(hnr.lower())

    ergebnis = defaultdict(list)
    for strasse, hausnummern in nummern_zuordnung.items():

        # Versuch, die Groß-/Kleinschreibung hübsch zu machen
        strasse = strasse.title()
        strasse = re.sub(r'(?<!\A)\bAn\b', 'an', strasse)
        strasse = re.sub(r'(?<!\A)\bDer\b', 'der', strasse)
        strasse = re.sub(r'(?<!\A)\bDes\b', 'des', strasse)

        ergebnis[strasse] = [str(g) for g in hausnummern_gruppieren(hausnummern)]

    return [
        f'{strasse} {", ".join(ergebnis[strasse])}'
        for strasse in sorted(ergebnis.keys())
    ]
