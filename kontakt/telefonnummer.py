__all__ = ['TelefonNummer']

import re


with open('kontakt/vorwahlen.txt', 'r') as vfile:
    VORWAHLEN = vfile.read().split(',')


class TelefonNummer:
    """
    Datenstruktur zum Strukturieren, Formatieren und Vergleichen
    von Telefonnummern in unterschiedlichen Formaten.

    Werden internationale bzw. nationale Vorwahl nicht angegeben,
    wird von (049 / Deutschland) bzw. (030 / Berlin) ausgegangen.
    """
    def __init__(self, nummer:str):
        self._int_vorwahl = '+49'
        self._vorwahl = '030'
        self._basisnummer = None
        self._nebenstelle = None

        nummer = self._nummer_bereinigen(nummer)

        # Landeskennzeichen (funktioniert nur für 2-stellige)
        if nummer.startswith('00'):
            self._int_vorwahl = f'+{nummer[2:4]}'
            nummer = '0' + nummer[4:]
        elif nummer.startswith('+'):
            self._int_vorwahl = nummer[:3]
            nummer = '0' + nummer[3:]
        
        # Vorwahl extrahieren
        if nummer.startswith('0'):
            for vorwahl in VORWAHLEN:
                if nummer.startswith(vorwahl):
                    self._vorwahl = vorwahl
            nummer = nummer[len(self._vorwahl):]

        # Nebenstelle filtern, falls vorhanden
        try:
            self._basisnummer, self._nebenstelle = nummer.split('-')
        except ValueError:
            self._basisnummer = nummer

    def __str__(self):
        return ''.join([
            self._int_vorwahl,
            self._vorwahl[1:],
            self._basisnummer,
            f'{self._nebenstelle or ""}'
        ])

    def __repr__(self):
        return f'TelefonNummer({self.international})'

    def __eq__(self, other: 'TelefonNummer'):
        return str(self) == str(other)

    @staticmethod
    def _nummer_gliedern(nummer):
        groups = [m[::-1] for m in re.findall(r'(\d{1,2})', nummer[::-1])[::-1]]
        if len(groups[0]) == 1:
            groups[1] = groups[0] + groups[1]
            del groups[0]
        return ' '.join(groups)

    @staticmethod
    def _nummer_bereinigen(nummer):
        """
        Filtert unerwünschte Zeichen aus der Darstellung.
        """
        return re.sub(r'(?:\s|\(0\)|\/|\(|\))', '', nummer)

    @property
    def basisnummer(self):
        return self._nummer_gliedern(self._basisnummer)

    @property
    def rufnummer(self):
        if self._nebenstelle:
            return f'{self._nummer_gliedern(self.basisnummer)} - {self._nebenstelle}'
        else:
            return self._nummer_gliedern(self.basisnummer)

    @property
    def rufnummer_mit_okz(self):
        return f'{self._vorwahl} {self.rufnummer}'

    @property
    def nebenstelle(self):
        return self._nebenstelle or None

    @property
    def vorwahl(self):
        return self._vorwahl

    @property
    def international(self):
        return ' '.join([
            self._int_vorwahl,
            self._vorwahl[1:],
            self.rufnummer
        ])
