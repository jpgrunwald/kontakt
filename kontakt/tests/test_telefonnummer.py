from ..telefonnummer import TelefonNummer


def test_nummer_im_internationalen_format_plus():
    tnr = TelefonNummer('+49301237899')
    assert tnr.vorwahl == '030'
    assert tnr._int_vorwahl == '+49'
    assert str(tnr) == '+49301237899'
    assert tnr.international == '+49 30 123 78 99'
    assert tnr.basisnummer == '123 78 99'
    assert tnr.rufnummer == '123 78 99'
    assert tnr.rufnummer_mit_okz == '030 123 78 99'
    assert tnr.nebenstelle is None


def test_nummer_im_internationalen_format_doppelnull():
    tnr = TelefonNummer('0049301237899')
    assert tnr.vorwahl == '030'
    assert tnr.basisnummer == '123 78 99'
    assert str(tnr) == '+49301237899'
    assert tnr.international == '+49 30 123 78 99'
    assert tnr.rufnummer == '123 78 99'
    assert tnr.rufnummer_mit_okz == '030 123 78 99'
    assert tnr.nebenstelle is None


def test_nummer_ohne_ortsvorwahl():
    tnr = TelefonNummer('1237899')
    assert str(tnr) == '+49301237899'
    assert tnr.international == '+49 30 123 78 99'
    assert tnr.basisnummer == '123 78 99'
    assert tnr.rufnummer == '123 78 99'
    assert tnr.rufnummer_mit_okz == '030 123 78 99'
    assert tnr.vorwahl == '030'
    assert tnr.nebenstelle is None


def test_dreistellige_vorwahl():
    tnr = TelefonNummer('033124 26')
    assert tnr.vorwahl == '0331'


def test_vierstellige_vorwahl():
    tnr = TelefonNummer('0049 2065 123 1124')
    assert tnr.vorwahl == '02065'
    assert tnr.basisnummer == '123 11 24'


def test_fuenfstellige_vorwahl():
    tnr = TelefonNummer('+493347472380')
    assert tnr.vorwahl == '033474'
    assert tnr.basisnummer == '723 80'


def test_vierstellige_rufnummer():
    tnr = TelefonNummer('030 7280')
    assert tnr.rufnummer == '72 80'


def test_nummer_ohne_landesvorwahl():
    tnr = TelefonNummer('0301237899')
    assert str(tnr) == '+49301237899'
    assert tnr.international == '+49 30 123 78 99'
    assert tnr.basisnummer == '123 78 99'
    assert tnr.rufnummer == '123 78 99'
    assert tnr.rufnummer_mit_okz == '030 123 78 99'
    assert tnr.vorwahl == '030'
    assert tnr.nebenstelle is None


def test_nummer_mit_leerzeichen():
    tnr = TelefonNummer('0049 30 1237899')
    assert str(tnr) == '+49301237899'
    assert tnr.international == '+49 30 123 78 99'
    assert tnr.basisnummer == '123 78 99'
    assert tnr.rufnummer == '123 78 99'
    assert tnr.nebenstelle is None


def test_nummer_mit_klammern():
    tnr = TelefonNummer('+49 (0) 30 1237899')
    assert str(tnr) == '+49301237899'
    assert tnr.international == '+49 30 123 78 99'
    assert tnr.basisnummer == '123 78 99'
    assert tnr.rufnummer == '123 78 99'
    assert tnr.nebenstelle is None


def test_nummer_mit_bindestrich():
    tnr = TelefonNummer('1237899-5')
    assert tnr.international == '+49 30 123 78 99 - 5'
    assert str(tnr) == '+493012378995'
    assert tnr.basisnummer == '123 78 99'
    assert tnr.rufnummer == '123 78 99 - 5'
    assert tnr.rufnummer_mit_okz == '030 123 78 99 - 5'
    assert tnr.nebenstelle == '5'


def test_nummer_mit_schraegstrich():
    tnr = TelefonNummer('030/1237899')
    assert str(tnr) == '+49301237899'
    assert tnr.international == '+49 30 123 78 99'
    assert tnr.basisnummer == '123 78 99'
    assert tnr.rufnummer_mit_okz == '030 123 78 99'
    assert tnr.nebenstelle is None


def test_vergleich_von_telefonnummern():
    assert TelefonNummer('723 80 719') == TelefonNummer('+49 3072380719')
    assert TelefonNummer('723 80 718') != TelefonNummer('723 80 719')
    assert TelefonNummer('723 80 - 719') == TelefonNummer('723 80 719')


def test_mobilfunknummer():
    tnr = TelefonNummer('0152 226 537 28')
    assert tnr.vorwahl == '01522'
    assert str(tnr) == '+4915222653728'
    assert tnr.rufnummer_mit_okz == '01522 265 37 28'
