from ..strassennamen_formatierer import strassennamen_zusammenfassen


def test_einfache_bereiche_zusammengefasst():
    test_daten = [
        'Mustergasse 3', 'Mustergasse 4', 'Mustergasse 5'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 3-5'
    ]

def test_einzelne_aufgaenge():
    assert strassennamen_zusammenfassen(['Mustergasse 3']) == ['Mustergasse 3']

def test_gemischt_bereiche_und_einzelne_aufgaenge():
    test_daten = [
        'Mustergasse 3', 'Mustergasse 4', 'Mustergasse 15'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 3-4, 15'
    ]

def test_strassennamen_mit_leerzeichen():
    test_daten = [
        'An der Kappe 1', 'An der Kappe 2', 'An der Kappe 3'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'An der Kappe 1-3'
    ]

def test_nur_ungerade_hnrn_einer_strasse():
    test_daten = [
        'Mustergasse 1', 'Mustergasse 3', 'Mustergasse 5'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 1-5 (unger.)'
    ]

def test_nur_gerade_hnrn_einer_strasse():
    test_daten = [
        'Mustergasse 2', 'Mustergasse 4', 'Mustergasse 6'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 2-6 (ger.)'
    ]

def test_hnrn_mit_buchstaben():
    test_daten = [
        'Mustergasse 2a', 'Mustergasse 2b', 'Mustergasse 2c'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 2a-c'
    ]

def test_hnrn_mit_buchstaben_unterschiedliche_basis():
    test_daten = [
        'Mustergasse 2a', 'Mustergasse 4b', 'Mustergasse 4c'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 2a, 4b-c'
    ]

def test_hnrn_mit_buchstaben_luecke_in_buchstabenfolge():
    test_daten = [
        'Mustergasse 2a', 'Mustergasse 2c'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 2a, 2c'
    ]

def test_unsortierte_datensaetze():
    test_daten = [
        'Mustergasse 4c',
        'Mustergasse 4b',
        'Mustergasse 3',
        'Mustergasse 2a',
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Mustergasse 2a, 3, 4b-c'
    ]

def test_abweichende_gross_kleinschreibung():
    test_daten = [
        'An der Kappe 1', 'An Der Kappe 2', 'an der kappe 3'
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'An der Kappe 1-3'
    ]

def test_strasse_am_wortende_abkuerzen():
    test_daten = ['Bergstrasse 3', 'Bergstraße 4']
    assert strassennamen_zusammenfassen(test_daten) == [
        'Bergstr. 3-4'
    ]

def test_strasse_am_wortanfang_nicht_abkuerzen():
    test_daten = ['Strassenreuther Weg 3', 'Straßenreuther Weg 4']
    assert strassennamen_zusammenfassen(test_daten) == [
        'Strassenreuther Weg 3', 'Straßenreuther Weg 4'
    ]

def test_strasse_im_wortinnern_nicht_abkuerzen():
    test_daten = ['Gestrassener Weg 3', 'Gestraßener Weg 4']
    assert strassennamen_zusammenfassen(test_daten) == [
        'Gestrassener Weg 3', 'Gestraßener Weg 4'
    ]

def test_eigenstaendiges_strasse_nicht_abkuerzen():
    test_daten = ['Straße des 17. Juni 3', 'Straße des 17. Juni 4']
    assert strassennamen_zusammenfassen(test_daten) == [
        'Straße des 17. Juni 3-4'
    ]

def test_eigenstaendiges_strasse_korrigieren_und_zusammenfassen():
    test_daten = ['Strasse des 17. Juni 3', 'Straße des 17. Juni 4']
    assert strassennamen_zusammenfassen(test_daten) == [
        'Straße des 17. Juni 3-4'
    ]

def test_strassennamen_sortiert_ausgeben():
    test_daten = [
        'Mustergasse 4c',
        'Alpenweg 3',
        'Mustergasse 4b',
        'Veitsgracht 13z',
        'Mustergasse 3',
        'Mustergasse 2a',
    ]
    assert strassennamen_zusammenfassen(test_daten) == [
        'Alpenweg 3',
        'Mustergasse 2a, 3, 4b-c',
        'Veitsgracht 13z'
    ]
